//
//  AppDelegate.h
//  RedCard
//
//  Created by Nick Galasso on 10/28/14.
//  Copyright (c) 2014 Nick Galasso. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

