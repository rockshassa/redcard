//
//  ViewController.m
//  RedCard
//
//  Created by Nick Galasso on 10/28/14.
//  Copyright (c) 2014 Nick Galasso. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic, strong) UIScrollView *scrollView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _scrollView = [UIScrollView new];
    _scrollView.pagingEnabled = YES;
    [self.view addSubview:_scrollView];
    
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
    for (UIView *v in self.scrollView.subviews) {
        [v removeFromSuperview];
    }
    
    CGRect cardSize = self.view.bounds;
    
    self.scrollView.frame = cardSize;
    
    UIView *redCard = [[UIView alloc] initWithFrame:cardSize];
    redCard.backgroundColor = [UIColor redColor];
    
    [self.scrollView addSubview:redCard];
    
    cardSize.origin.x = CGRectGetMaxX(redCard.frame);
    
    UIView *greenCard = [[UIView alloc] initWithFrame:cardSize];
    greenCard.backgroundColor = [UIColor greenColor];
    
    [self.scrollView addSubview:greenCard];
    
    self.scrollView.contentSize = CGSizeMake(CGRectGetMaxX(greenCard.frame), 0);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
